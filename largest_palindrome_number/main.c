#include <stdio.h>

int pal(int n){
    int res = 0;
    while(n > 0){
        int d = n %10;
        n /= 10;
        res = res * 10 + d;
    }
    return res;
}

int main(int ac, char** av){
    int res = 0;
    for(int i = 999; i > 0; i--){
        for (int j = 999; j > 0; j--){
            if(pal(i*j) == i*j && i*j > res){
                res = i * j;
            }
        }
    }
    printf("%d\n",res);
    return 0;
}