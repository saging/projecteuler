#include <stdio.h>
/*
a(n) = 4*a(n-1) + a(n-2), with a(-1) = 2, a(0) = 0
from 
https://oeis.org/A014445
*/
int main(int ac, char** av){
    int res = 0;
    int a=2, b=0, c;
    while(b < 4000000){
        c = a+4*b;
        a = b;
        b = c;
        res += a;
    }
    printf("%d\n", res);
    return 0;
}