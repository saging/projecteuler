#include <stdio.h>

int main(int ac, char ** av){
    __uint128_t goal = 600851475143;
    __uint128_t lpf = 2;
    while(goal > 1){
        __uint128_t divide = goal / lpf;

        if(goal == lpf * divide)
            goal = goal / lpf;
        else
            lpf++;
    }
    printf("%d\n",lpf);
    return 0;
}